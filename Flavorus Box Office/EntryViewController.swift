//
//  FirstViewController.swift
//  Ticketon Box Office
//
//  Created by admin on 4/28/15.
//  Copyright (c) 2015 Ticketon. All rights reserved.
//

import UIKit
import SwiftyJSON

class EntryViewController: UIViewController {

    @IBOutlet weak var ManualTextField: UITextField!
    @IBOutlet weak var ScanResultsView: UIView!
    
    var authToken: String = ""
    var events: [JSON] = []
    var scansettings:API.scansettings = API.scansettings()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // init linea class and connect it
        let scanner : DTDevices = DTDevices()
        scanner.addDelegate(self)
        scanner.connect()
 
        
                
        // Do any additional setup after loading the view, typically from a nib.
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let key = "AuthToken"
        authToken = (NSUserDefaults.standardUserDefaults().objectForKey(key)) as! String
        
        if authToken == "" //Check for first run of app
        {
            //bad!!
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainController = storyboard.instantiateViewControllerWithIdentifier("Login") as! UIViewController
            appDelegate.window?.rootViewController = mainController
            return
        }

        if appDelegate.events != nil {
        self.events = appDelegate.events!
        scansettings.events = getEvents()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func SettingsButtonPushed(sender: AnyObject) {
        if let viewWithTag = self.view.viewWithTag(100) {
            let xPosition = viewWithTag.frame.origin.x
            let yPosition = -self.view.frame.size.height
            let height = viewWithTag.frame.size.height
            let width = viewWithTag.frame.size.width
            UIView.animateWithDuration(0.5, animations: {
                viewWithTag.frame = CGRectMake(xPosition, yPosition, height, width)
                }, completion: {
                    (value: Bool) in
                    viewWithTag.removeFromSuperview()
                }
            )
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let settingsView = storyboard.instantiateViewControllerWithIdentifier("EntrySettings") as! UIViewController
            addChildViewController(settingsView)
            settingsView.view.tag = 100
            let xPosition = settingsView.view.frame.origin.x
            let yPosition = settingsView.view.frame.origin.y
            let height = settingsView.view.frame.size.height
            let width = settingsView.view.frame.size.width
            settingsView.view.frame = CGRectMake(xPosition, -self.view.frame.size.height, height, width)
            self.view.addSubview(settingsView.view)
            settingsView.didMoveToParentViewController(self)
            UIView.animateWithDuration(0.5, animations: {
                settingsView.view.frame = CGRectMake(xPosition, yPosition, height, width)
            })
        }
    }
    
    
    func barcodeData (barcode: String, type: Int) {
    
    // You can use this data as you wish
    // Here I write barcode data into the console
        //println("Barcode Data: \(barcode)")
        ScanTicket(barcode)
    }
    
    @IBAction func ScanBarcodeButton(sender: AnyObject) {
        // this is the camera view
        let barcodeViewController:BarcodeImageScannerViewController = BarcodeImageScannerViewController()
        barcodeViewController.scansettings = scansettings
        barcodeViewController.authToken = authToken
        
        self.presentViewController(barcodeViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func ManualButtonTouchUp(sender: AnyObject) {
        let ticketnum = self.ManualTextField.text
        if (ticketnum != "")
        {
            println(ticketnum)
            ScanTicket(ticketnum)
        }
    }
    
    func ScanTicket(ticketnum: String){
        var api = API()
        
        api.scanTicket(authToken, ticketnum: ticketnum, scanSettings: scansettings, callback: { (data, error) in
            if (error == nil) {
                
                if let viewWithTag = self.ScanResultsView.viewWithTag(100) {
                    viewWithTag.removeFromSuperview()
                }
                
                // do stuff with data
                if (data["success"] == false)
                {
                    // bad scan
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let scanResultsController = storyboard.instantiateViewControllerWithIdentifier("EntryScanResultsTable") as! EntryScanResultsViewController
                    scanResultsController.tableView.backgroundColor = UIColor .clearColor()
                    scanResultsController.items.append(data)
                    let scanview = scanResultsController.view
                    self.ScanResultsView.addSubview(scanview)
                    
                }else{
                    
                    //show subview
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let scanResultsController = storyboard.instantiateViewControllerWithIdentifier("EntryScanResultsTable") as! EntryScanResultsViewController
                    scanResultsController.tableView.backgroundColor = UIColor .clearColor()
                    for (key, ticketjson) in data["tickets"] {
                        scanResultsController.items.append(ticketjson)
                    }
                    let scanview = scanResultsController.view
                    self.ScanResultsView.addSubview(scanview)
                    
                }
            }
        })

    }
    
    func getEvents() -> String {
        var eventstring = ""
        for (eventjson) in self.events {
            if (eventstring != "") { eventstring = eventstring + "," }
            eventstring += eventjson["ID"].stringValue
        }
        return eventstring
    }
    
}

