//
//  EventTableViewController.swift
//  Flavorus Box Office
//
//  Created by admin on 5/22/15.
//  Copyright (c) 2015 Flavorus. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreData

class EventTableViewController: UITableViewController, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate  {
    var items: [JSON] = []
    var filteredItems = [JSON]()
    var searchActive : Bool = false
    var wipeCodeData: Bool = false
    var selectedItems: [JSON] = []

    var authToken: String = ""
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBAction func SaveButtonPushed(sender: AnyObject) {
        for eventjson in self.selectedItems {
            let eventid = eventjson["ID"].stringValue
            println("You selected event #\(eventid)!")
            
            saveEventsToCoreData(eventjson)
        }
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.events = self.selectedItems
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = storyboard.instantiateViewControllerWithIdentifier("TabBar") as! UIViewController
        appDelegate.window!.rootViewController = mainController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        
        let key = "AuthToken"
        authToken = (NSUserDefaults.standardUserDefaults().objectForKey(key)) as! String

        if authToken == "" //Check for first run of app
        {
          //bad!!
          let storyboard = UIStoryboard(name: "Main", bundle: nil)
          let mainController = storyboard.instantiateViewControllerWithIdentifier("Login") as! UIViewController
          let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
          appDelegate.window?.rootViewController = mainController
          return
        }
        
        
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if self.wipeCodeData {
            appDelegate.clearCoreDataEvents()
        }

        
        if let eventsCD = appDelegate.getCoreDataEvents() {
            var isThereValidEvents:Bool = false
            
            for result in eventsCD as [NSManagedObject] {
                if result.valueForKey("event") as? String != "" {
                    isThereValidEvents = true
                }
            }
            if isThereValidEvents {
                refreshTableFromAPI(self, callIfSuccess: {
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    var events:[JSON] = [JSON]()
                    for eventCD in eventsCD as [NSManagedObject] {
                        for jevent in self.items {
                            if eventCD.valueForKey("event") as? String == jevent["ID"].stringValue {
                                events.append(jevent)
                            }
                        }
                    }
                    appDelegate.events = events
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let mainController = storyboard.instantiateViewControllerWithIdentifier("TabBar") as! UIViewController
                    appDelegate.window!.rootViewController = mainController
                })
            }else{
                refreshTableFromAPI(self, callIfSuccess: refreshTableIfItems)
            }
            
        }else{
            refreshTableFromAPI(self, callIfSuccess: refreshTableIfItems)
        }
        
        var refreshControl = UIRefreshControl()
        self.refreshControl = refreshControl
        
        self.refreshControl?.addTarget(self, action: "refreshTableFromAPI:", forControlEvents: UIControlEvents.ValueChanged)
        
        
    }

    func refreshTableFromAPI(sender:AnyObject, callIfSuccess: () -> Void)
    {
        var api = API()
        api.getEventsFull(authToken, callback: { (data, error) in
            if (error == nil) {
                // do stuff with data
                if (data["success"] == false)
                {
                    let messagestr = data["message"].stringValue
                    
                    var alert = UIAlertController(title: "Error", message: messagestr, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "close", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                    
                }else{
                    self.items = []
                    for (key, eventjson) in data["Events"] {
                        self.items.append(eventjson)
                    }
                    
                    callIfSuccess()
                }
            }
        })
        
        self.refreshControl?.endRefreshing()
    }
    
    func refreshTableIfItems() {
        if (self.items.count > 0) {
            self.tableView.reloadData()
        }
    }
    
    
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.filteredItems = self.items.filter({( eventJson: JSON) -> Bool in
            let stringMatch = eventJson["Name"].stringValue.lowercaseString.rangeOfString(searchText.lowercaseString)
            return (stringMatch != nil)
        })
        if(searchText == ""){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tableView.reloadData()
    }

    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.searchActive) {
            return self.filteredItems.count
        } else {
            return self.items.count
        }
    }
    
override     
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:EventTableCell = self.tableView.dequeueReusableCellWithIdentifier("EventTableCell") as! EventTableCell
        
        
        var jsonEvent:JSON
        
        if (self.searchActive) {
            jsonEvent = self.filteredItems[indexPath.row]
        } else {
            jsonEvent = self.items[indexPath.row]
        }
        
        cell.parentController = self
        cell.jsonEvent = jsonEvent
        cell.EventNameText.text = jsonEvent["Name"].stringValue
        cell.VenueText.text = jsonEvent["Venue"].stringValue
        println(jsonEvent["StartDate"].stringValue)
        cell.FullDateText.text = jsonEvent["StartDate"].stringValue
        
        if (jsonEvent["IsPassEvent"].stringValue == "true"){
            cell.PassImg1.alpha = 1.0
            cell.PassImg2.alpha = 1.0
            cell.PassImg3.alpha = 1.0
        }else{
            cell.PassImg1.alpha = 0.0
            cell.PassImg2.alpha = 0.0
            cell.PassImg3.alpha = 0.0
        }
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "M/d/yyyy hh:mm:ss aa" /*find out and place date format from http://userguide.icu-project.org/formatparse/datetime*/
        let date = dateFormatter.dateFromString(jsonEvent["StartDate"].stringValue)
        
        dateFormatter.dateFormat = "dd"
        cell.DateDayText.text = dateFormatter.stringFromDate(date!)
        
        dateFormatter.dateFormat = "MMM"
        cell.DateMonthText.text = dateFormatter.stringFromDate(date!)
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var jsonEvent:JSON

        if (self.searchActive) {
            jsonEvent = self.filteredItems[indexPath.row]
        } else {
            jsonEvent = self.items[indexPath.row]
        }
    }

    func saveEventsToCoreData(jevent: JSON) {
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        let entity =  NSEntityDescription.entityForName("Events",
            inManagedObjectContext:
            managedContext)
        
        let eventCD = NSManagedObject(entity: entity!,
            insertIntoManagedObjectContext:managedContext)
        
        //3
        eventCD.setValue(jevent["ID"].stringValue, forKey: "event")
    
        
        //4
        var error: NSError?
        if !managedContext.save(&error) {
            println("Could not save \(error), \(error?.userInfo)")
        }

    }
}
