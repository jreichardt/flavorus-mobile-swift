//
//  EntryScanTableCell.swift
//  Flavorus Box Office
//
//  Created by James Reichardt on 5/23/15.
//  Copyright (c) 2015 Flavorus. All rights reserved.
//

import UIKit
import SwiftyJSON


class EntryScanTableCell: UITableViewCell {

    @IBOutlet weak var TicketTypeNameText: UILabel!

    @IBOutlet weak var DateText: UILabel!
    @IBOutlet weak var DateTextDuplicate: UILabel!
    @IBOutlet weak var DateTextFailure: UILabel!
    @IBOutlet weak var TicketTypeNameTextDuplicate: UILabel!

    @IBOutlet weak var SecText: UILabel!
    @IBOutlet weak var RowText: UILabel!
    @IBOutlet weak var SeatText: UILabel!
    @IBOutlet weak var ScanTimeText: UILabel!
    @IBOutlet weak var ScanByText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
