//
//  EventTableCell.swift
//  Flavorus Box Office
//
//  Created by Lydia Reichardt on 5/28/15.
//  Copyright (c) 2015 Flavorus. All rights reserved.
//


import UIKit
import SwiftyJSON


class EventTableCell: UITableViewCell {
    
    var jsonEvent: JSON = []
    var isEventSelected: Bool = false
    var parentController:EventTableViewController?
   
    @IBOutlet weak var DateMonthText: UILabel!
    @IBOutlet weak var DateDayText: UILabel!
    @IBOutlet weak var EventNameText: UILabel!
    @IBOutlet weak var VenueText: UILabel!
    @IBOutlet weak var FullDateText: UILabel!
    
    @IBOutlet weak var PassImg1: UIView!
    @IBOutlet weak var PassImg2: UIView!
    @IBOutlet weak var PassImg3: UIView!
    @IBOutlet weak var EventButton: UIButton!
    
    
    @IBAction func eventButtonPushed(sender: AnyObject) {
        if isEventSelected {
            //--- uncheck
            self.EventButton.setImage(UIImage(named: "EventCheckGray"), forState: UIControlState.Normal)
            //--- remove
            let indexOfItem = getIndexOfSelectedItem()
            if indexOfItem >= 0 {
                self.parentController!.selectedItems.removeAtIndex(indexOfItem)
            }
        }else{
            self.EventButton.setImage(UIImage(named: "EventCheckRed"), forState: UIControlState.Normal)
            self.parentController!.selectedItems.append(jsonEvent)
        }
        isEventSelected != isEventSelected
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func getIndexOfSelectedItem() -> Int {
        var cindex:Int = 0
        for jevent in self.parentController!.selectedItems {
            if jevent["ID"].stringValue == self.jsonEvent["ID"].stringValue {
                return cindex
            }
            cindex += 1
        }
        return -1
    }
    
}

