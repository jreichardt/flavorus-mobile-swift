//
//  EventTableViewController.swift
//  Flavorus Box Office
//
//  Created by admin on 5/22/15.
//  Copyright (c) 2015 Flavorus. All rights reserved.
//

import UIKit
import SwiftyJSON


class EntryScanResultsViewController: UITableViewController, UITableViewDelegate, UITableViewDataSource  {
    var items                   : [JSON] = []
    var startBarcodeDeligate    : StartBarcodeDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundColor = UIColor .clearColor()
        //self.tableView.registerClass(EntryScanTableCell.self, forCellReuseIdentifier: "cell")
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        
        
        UIView.animateWithDuration(0.25, animations: {
            self.tableView.alpha = 0.0
            }, completion: { finished in
                
                UIView.animateWithDuration(0.25, animations: {
                    self.tableView.alpha = 1.0
                    }, completion: { finished in
                        UIView.animateWithDuration(0.25, animations: {
                            self.tableView.alpha = 0.0
                            }, completion: { finished in
                                
                                UIView.animateWithDuration(0.25, animations: {
                                    self.tableView.alpha = 1.0
                                    }, completion: { finshed in
                                         self.startBarcodeDeligate?.StartRunningBarcodeSession(self)
                                })
                        })
                })
        })
        
        //self.dismissViewControllerAnimated(false, completion: nil)
        
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }
    
    override
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:EntryScanTableCell
        let jsonItem = self.items[indexPath.row]
        
        let date = NSDate();
        let dateFormatter = NSDateFormatter()
        //To prevent displaying either date or time, set the desired style to NoStyle.
        dateFormatter.locale = NSLocale.currentLocale() //Set local language
        dateFormatter.timeStyle = NSDateFormatterStyle.MediumStyle //Set time style
        dateFormatter.dateStyle = NSDateFormatterStyle.NoStyle //Set date style
        dateFormatter.timeZone = NSTimeZone()
        let localDate = dateFormatter.stringFromDate(date)
        
        if (jsonItem["success"] == false) {
            cell = self.tableView.dequeueReusableCellWithIdentifier("FailureScan") as! EntryScanTableCell
            cell.DateTextFailure.text = localDate
        }else{
            if (jsonItem["ss"]==""){
                cell = self.tableView.dequeueReusableCellWithIdentifier("SuccessfulScan") as! EntryScanTableCell
                let sec = jsonItem["SectionName"].stringValue
                let seat = jsonItem["SeatName"].stringValue
                let row = jsonItem["RowName"].stringValue
                cell.SecText.text = sec
                cell.RowText.text = row
                cell.SeatText.text = seat
                cell.DateText.text = localDate
                cell.TicketTypeNameText.text = jsonItem["tt"].stringValue
            }else{
                cell = self.tableView.dequeueReusableCellWithIdentifier("DuplicateScan") as! EntryScanTableCell
                let lastswipe = jsonItem["lastswipe"].stringValue.stringByReplacingOccurrencesOfString("Scan: ", withString: "")
                cell.ScanTimeText.text = lastswipe
                cell.ScanByText.text = jsonItem["whoswiped"].stringValue
                cell.DateTextDuplicate.text = localDate
                cell.TicketTypeNameTextDuplicate.text = jsonItem["tt"].stringValue
            }
            
        }
        
        
        return cell
    }
    
        
}
