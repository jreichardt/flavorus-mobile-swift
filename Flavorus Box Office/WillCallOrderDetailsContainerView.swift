//
//  WillCallOrderDetailsContainerView.swift
//  Flavorus Box Office
//
//  Created by admin on 6/9/15.
//  Copyright (c) 2015 Flavorus. All rights reserved.
//


import UIKit
import SwiftyJSON

class WillCallOrderDetailsContainerView: UIViewController {
    
   
    @IBOutlet weak var TableViewContainer: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var last4: UILabel!
    
    var nameString: String = ""
    var last4String: String = ""
    var items: [JSON] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.text = nameString
        last4.text = last4String

        // create the table view controller
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let orderDetailsTable = storyboard.instantiateViewControllerWithIdentifier("WillCallOrderDetailsTable") as! WillCallOrderDetailsController
        orderDetailsTable.items = self.items
        self.TableViewContainer.addSubview(orderDetailsTable.view)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

