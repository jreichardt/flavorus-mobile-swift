//
//  WillcallTabViewController.swift
//  Flavorus Box Office
//
//  Created by admin on 6/5/15.
//  Copyright (c) 2015 Flavorus. All rights reserved.
//


import UIKit
import SwiftyJSON

class WillcallTabViewController: UIViewController {
    
    @IBOutlet weak var WillcallTableView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let willCallController = storyboard.instantiateViewControllerWithIdentifier("WillcallTableView") as! WillCallController
        let willcallview = willCallController.view
        self.WillcallTableView.addSubview(willcallview)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}