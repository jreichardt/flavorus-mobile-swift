//
//  WillCallOrderDetailsCell.swift
//  Flavorus Box Office
//
//  Created by admin on 6/8/15.
//  Copyright (c) 2015 Flavorus. All rights reserved.
//


import UIKit
import SwiftyJSON


class WillCallOrderDetailsCell: UITableViewCell {
    
    
    @IBOutlet weak var TicketType: UILabel!
    @IBOutlet weak var Quantity: UILabel!
    @IBOutlet weak var QuantBoxView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

