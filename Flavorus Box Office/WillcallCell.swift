//
//  Willcall Cell.swift
//  Flavorus Box Office
//
//  Created by Lydia Reichardt on 5/28/15.
//  Copyright (c) 2015 Flavorus. All rights reserved.
//


import UIKit
import SwiftyJSON


class WillcallCell: UITableViewCell {
    
    
    @IBOutlet weak var NameText: UILabel!
    @IBOutlet weak var Last4: UILabel!
    @IBOutlet weak var EventName: UILabel!
    @IBOutlet weak var Quantity: UILabel!
    @IBOutlet weak var QuantBoxView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

