//
//  EntrySettingsController.swift
//  Flavorus Box Office
//
//  Created by James Reichardt on 6/10/15.
//  Copyright (c) 2015 Flavorus. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreData

class EntrySettingsController: UIViewController {
    
    @IBOutlet weak var Name: UILabel!
   
    @IBAction func SignOutButton(sender: AnyObject) {
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        //--- blow out old data
        appDelegate.clearCoreDataUser()
        appDelegate.clearCoreDataEvents()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = storyboard.instantiateViewControllerWithIdentifier("Login") as! UIViewController
        appDelegate.window?.rootViewController = mainController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let user = appDelegate.getCoreDataUser()
        Name.text = user!.valueForKey("name") as? String
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}









